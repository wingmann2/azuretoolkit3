import { Component } from '@angular/core';
import { DateService } from '../common/services/date.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  constructor (private dateService: DateService) {
    
  }
}
