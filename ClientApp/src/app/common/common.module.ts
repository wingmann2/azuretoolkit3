import { NgModule } from '@angular/core';
import { CognitiveService } from './services/cognitive.service';
import { AzureHttpClient } from './services/azureHttpClient';
import { DateService } from './services/date.service';

@NgModule({
    providers: [AzureHttpClient, CognitiveService, DateService]
})

export class CommonModule { }